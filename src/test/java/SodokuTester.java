import org.assertj.core.api.BDDAssertions;
import org.junit.Test;

public class SodokuTester {
    @Test
    public void it_should_solve_easy_puzzle() {
        Sodoku sodoku = new Sodoku();

        //        0 0 3  9 0 0  0 5 1
        //        5 4 6  0 1 8  3 0 0
        //        0 0 0  0 0 7  4 2 0
        //
        //        0 0 9  0 5 0  0 3 0
        //        2 0 0  6 0 3  0 0 4
        //        0 8 0  0 7 0  2 0 0
        //
        //        0 9 7  3 0 0  0 0 0
        //        0 0 1  8 2 0  9 4 7
        //        8 5 0  0 0 4  6 0 0

        Board actual = sodoku.solve(new int[][] { { 0, 0, 3, 9, 0, 0, 0, 5, 1 }, { 5, 4, 6, 0, 1, 8, 3, 0, 0 }, { 0, 0, 0, 0, 0, 7, 4, 2, 0 }, { 0, 0, 9, 0, 5, 0, 0, 3, 0 }, { 2, 0, 0, 6, 0, 3, 0, 0, 4 }, { 0, 8, 0, 0, 7, 0, 2, 0, 0 }, { 0, 9, 7, 3, 0, 0, 0, 0, 0 }, { 0, 0, 1, 8, 2, 0, 9, 4, 7 }, { 8, 5, 0, 0, 0, 4, 6, 0, 0 } });
        System.out.println(actual);
        BDDAssertions.then(sodoku.isValid(actual)).isTrue();
    }

    @Test
    public void it_should_solve_hard_to_brute_force_puzzle() {
        Sodoku sodoku = new Sodoku();

        // 0 0 0  0 0 0  0 0 0
        // 0 0 0  0 0 3  0 8 5
        // 0 0 1  0 2 0  0 0 0

        // 0 0 0  5 0 7  0 0 0
        // 0 0 4  0 0 0  1 0 0
        // 0 9 0  0 0 0  0 0 0

        // 5 0 0  0 0 0  0 7 3
        // 0 0 2  0 1 0  0 0 0
        // 0 0 0  0 4 0  0 0 9

        Board actual = sodoku.solve(new int[][] { { 0, 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 3, 0, 8, 5 }, { 0, 0, 1, 0, 2, 0, 0, 0, 0 }, { 0, 0, 0, 5, 0, 7, 0, 0, 0 }, { 0, 0, 4, 0, 0, 0, 1, 0, 0 }, { 0, 9, 0, 0, 0, 0, 0, 0, 0 }, { 5, 0, 0, 0, 0, 0, 0, 7, 3 }, { 0, 0, 2, 0, 1, 0, 0, 0, 0 }, { 0, 0, 0, 0, 4, 0, 0, 0, 9 } });
        System.out.println(actual);
        BDDAssertions.then(sodoku.isValid(actual)).isTrue();
    }

    @Test
    public void it_should_solve_medium_puzzle() {
        Sodoku sodoku = new Sodoku();

        // 5 0 0  0 0 0  0 0 0
        // 6 0 0  8 0 0  9 0 3
        // 1 0 0  0 6 0  0 0 0

        // 0 0 0  0 0 7  0 0 0
        // 0 0 9  0 5 0  8 6 4
        // 0 0 1  2 0 0  0 0 0

        // 0 0 4  0 0 0  7 0 2
        // 0 0 2  7 0 0  4 0 0
        // 0 0 0  0 4 0  0 0 6

        Board actual = sodoku.solve(new int[][] { { 5, 0, 0, 0, 0, 0, 0, 0, 0 }, { 6, 0, 0, 8, 0, 0, 9, 0, 3 }, { 1, 0, 0, 0, 6, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 7, 0, 0, 0 }, { 0, 0, 9, 0, 5, 0, 8, 6, 4 }, { 0, 0, 1, 2, 0, 0, 0, 0, 0 }, { 0, 0, 4, 0, 0, 0, 7, 0, 2 }, { 0, 0, 2, 7, 0, 0, 4, 0, 0 }, { 0, 0, 0, 0, 4, 0, 0, 0, 6 } });
        System.out.println(actual);
        BDDAssertions.then(sodoku.isValid(actual)).isTrue();
    }
}
