public class ChangeEvent {
    public int value;
    public boolean iAm;

    public ChangeEvent(int value, boolean iAm) {
        this.iAm = iAm;
        this.value = value;
    }
}