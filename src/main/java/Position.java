public class Position {
    private int column;
    private int row;

    public Position(int column, int row) {
        this.column = column;
        this.row = row;
    }

    public boolean equals(Object other) {
        if (!(other instanceof Position))
            return false;
        return ((Position)other).column == column && ((Position)other).row == row;
    }

    public String toString() {
        return "[" + column + ", " + row + "]";
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }
}