public class Board {
    public Node[][] board;

    public Board() {
        board = new Node[9][9];
        for (int column = 0; column < board.length; column++)
            for (int row = 0; row < board[column].length; row++)
                board[column][row] = new Node(new Position(column, row));
        for (int column = 0; column < board.length; column++)
            for (int row = 0; row < board[column].length; row++)
                board[column][row].setUpObservers(this);
    }

    public Node getAt(int column, int row) {
        return board[column][row];
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int column = 0; column < board.length; column++) {
            for (int row = 0; row < board[column].length; row++) {
                if (row % 3 == 0)
                    stringBuilder.append(" ");
                stringBuilder.append(board[column][row].value);
                stringBuilder.append(" ");
            }
            if (column % 3 == 2)
                stringBuilder.append("\n");
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
}
