public class Groups {
    public int THREASHOLD = 8;
    public int NOT_POSSIBLE = -1;
    public int VERTICAL = 0;
    public int HORIZONTAL = 1;
    public int LOCAL = 2;
    public int[][] possibilities;

    public Groups() {
        this.possibilities = new int[9][3];
    }

    public boolean canSet(int value) {
        return possibilities[value - 1][VERTICAL] == THREASHOLD || possibilities[value - 1][HORIZONTAL] == THREASHOLD || possibilities[value - 1][LOCAL] == THREASHOLD;
    }

    public void incrementPossibility(int value, Node from, Node to) {
        if (from.position.getColumn() == to.position.getColumn())
            possibilities[value - 1][HORIZONTAL]++;
        if (from.position.getRow() == to.position.getRow())
            possibilities[value - 1][VERTICAL]++;
        if (isInLocal(from, to))
            possibilities[value - 1][LOCAL]++;
    }

    public boolean isInLocal(Node from, Node to) {
        Position fromStart = new Position(from.position.getColumn() - (from.position.getColumn() % 3 == 0 ? 0 : from.position.getColumn() % 3), from.position.getRow() - (from.position.getRow() % 3 == 0 ? 0 : from.position.getRow() % 3));
        Position toStart = new Position(to.position.getColumn() - (to.position.getColumn() % 3 == 0 ? 0 : to.position.getColumn() % 3), to.position.getRow() - (to.position.getRow() % 3 == 0 ? 0 : to.position.getRow() % 3));
        return fromStart.equals(toStart);
    }

    public boolean isPossible(int value) {
        return possibilities[value - 1][VERTICAL] != NOT_POSSIBLE && possibilities[value - 1][HORIZONTAL] != NOT_POSSIBLE && possibilities[value - 1][LOCAL] != NOT_POSSIBLE;
    }

    public void setNotPossible(int value) {
        possibilities[value - 1][VERTICAL] = NOT_POSSIBLE;
        possibilities[value - 1][HORIZONTAL] = NOT_POSSIBLE;
        possibilities[value - 1][LOCAL] = NOT_POSSIBLE;
    }

    public String toString() {
        StringBuilder strB = new StringBuilder();
        for (int i = 0; i < possibilities.length; i++) {
            strB.append(possibilities[i][0] + "" + possibilities[i][1] + possibilities[i][2] + " ");
        }
        return strB.toString().replace("-1", "_");
    }
}
