import java.util.Observable;
import java.util.Observer;

public class Node extends Observable implements Observer {
    public int value;
    public Groups groups;
    public Position position;

    public Node(Position position) {
        groups = new Groups();
        this.position = position;
    }

    public boolean found() {
        return value != 0;
    }

    public void set(int value) {
        this.value = value;
        setChanged();
        notifyObservers(new ChangeEvent(value, true));
        for (int i = 1; i <= 9; i++) {
            if (i != value && groups.isPossible(i)) {
                setChanged();
                notifyObservers(new ChangeEvent(i, false));
            }
        }
    }

    public void setUpObservers(Board board) {
        Node[][] b = board.board;
        for (int column = 0; column < b.length; column++) {
            if (column != position.getColumn()) {
                b[column][position.getRow()].addObserver(this);
            }
        }
        for (int row = 0; row < b.length; row++) {
            if (row != position.getRow()) {
                b[position.getColumn()][row].addObserver(this);
            }
        }
        Position start = new Position(position.getColumn() - (position.getColumn() % 3 == 0 ? 0 : position.getColumn() % 3), position.getRow() - (position.getRow() % 3 == 0 ? 0 : position.getRow() % 3));
        if (position.equals(new Position(0, 2)))
            System.out.print("");
        for (int column = start.getColumn(); column < start.getColumn() + 3; column++) {
            for (int row = start.getRow(); row < start.getRow() + 3; row++) {
                if (column != position.getColumn() && row != position.getRow())
                    b[column][row].addObserver(this);
            }
        }
    }

    public String toString() {
        return found() ? value + "" : "_";
    }

    @Override
    public void update(Observable o, Object arg) {
        Node from = (Node)o;
        ChangeEvent wasTold = (ChangeEvent)arg;

        if (from.position.equals(new Position(2, 4)) && position.equals(new Position(0, 4)) && wasTold.value == 4)
            System.out.println();

        int value = wasTold.value;
        if (!found() && groups.isPossible(value)) {
            if (wasTold.iAm) {
                groups.setNotPossible(value);
                setChanged();
                notifyObservers(new ChangeEvent(value, false));
            } else {
                if (groups.possibilities[0][0] + 1 == 4 && position.equals(new Position(3, 1)) && value == 1)
                    System.out.println();
                groups.incrementPossibility(value, from, this);
                if (groups.canSet(value))
                    set(value);
            }
        }
    }
}