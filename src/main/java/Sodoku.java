public class Sodoku {
    public Board generate() {
        return null;
    }

    public Board generate(String difficulty) {
        return null;
    }

    public Board[] generateMultiple(String difficulty) {
        return null;
    }

    public boolean isValid(Board board) {
        return false;
    }

    /**
     * @param positionValues expected format {{ column, row, value }}
     */
    public Board solve(int[][] positionValues) {
        Board board = new Board();
        for (int column = 0; column < positionValues.length; column++) {
            for (int row = 0; row < positionValues[column].length; row++) {
                if (positionValues[column][row] != 0)
                    board.getAt(column, row).set(positionValues[column][row]);
            }
        }
        return board;
    }
}
